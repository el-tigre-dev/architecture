import 'package:architecture/screens/home.dart';
import 'package:flutter/material.dart';

class Architecture extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Architecture',
      home: Home(),
    );
  }
}
