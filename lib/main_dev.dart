import 'package:flutter/material.dart';

import './app_config.dart';
import './architecture.dart';

void main() => runApp(AppConfig(
      debug: true,
      apiBaseUrl: 'http://192.168.1.16:80/api',
      child: Architecture(),
    ));
