import 'package:flutter/material.dart';

import './app_config.dart';
import './architecture.dart';

void main() => runApp(AppConfig(
      debug: false,
      apiBaseUrl: 'https://domain_name/api',
      child: Architecture(),
    ));
