import 'package:architecture/widgets/current_stage_data.dart';
import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CurrentStageData(),
      ),
    );
  }
}
