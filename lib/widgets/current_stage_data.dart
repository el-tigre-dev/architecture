import 'package:architecture/app_config.dart';
import 'package:flutter/material.dart';

class CurrentStageData extends StatelessWidget {
  Widget build(BuildContext context) {
    AppConfig config = AppConfig.of(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(bottom: 16),
          child: Text('API BASE URL: ${config.apiBaseUrl}'),
        ),
        Text('DEBUG: ${config.debug}')
      ],
    );
  }
}
